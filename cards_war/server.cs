﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Windows.Forms;

namespace cards_war
{
    class server
    {
        //server = connect , send , recv , close . 
        public byte[] _bufferout;
        public byte[] _bufferIN;
      
        NetworkStream clientstraem = null;
        TcpClient client = new TcpClient();

        public void connect()
        {
            client = new TcpClient();
            IPEndPoint ServerEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            try
            {
                client.Connect(ServerEndPoint);
            }
            catch
            {
                MessageBox.Show("the server is under attack , he not active , \nplease try again in 2 days ","ERROR");
                Environment.Exit(0);
            }

            clientstraem = client.GetStream();
        }

        public void send(string send)
        {
            _bufferout = new ASCIIEncoding().GetBytes(send);
            clientstraem.Write(_bufferout, 0, _bufferout.Length);
            clientstraem.Flush();

        }

        public string recv()
        {
            _bufferIN = new byte[4];
            int bufferread = clientstraem.Read(_bufferIN, 0, 4);
            string recv = new ASCIIEncoding().GetString(_bufferIN);
            return recv; 
        }

        public void close()
        {
            clientstraem.Close();
            client.Close();
        }


    }
}
