﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cards_war
{
    public class playcard
    {


        public int _value;
        public string _shape;
        public string _color;
        public bool ifwin = true;
     

        public playcard(int value, string shape, string color)
        {
            _value = value;
            _shape = shape;
            _color = color;

        }

        public static bool operator < (playcard s1,playcard s2)
        {

            if (s1._value < s2._value)
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        public static bool operator >(playcard s1, playcard s2)
        {

            if (s1._value > s2._value)
            {
                return true;
            }
            else
            {
                return false;
            }


        }
        public static bool operator ==(playcard s1, playcard s2)
        {

            if (s1._value == s2._value)
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        public static bool operator !=(playcard s1, playcard s2)
        {

            if (s1._value != s2._value)
            {
                return true;
            }
            else
            {
                return false;
            }


        }


        
      
       



}
}
