﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace cards_war
{
    public partial class CardsBoard : Form
    {
        server Server = new server();//my server class /..
        public static bool ifexit = false;//if was exit dont read or do somthing
        public string _recv;//recv from server
        PictureBox _p;//thread
        int _my_value;//score
        int _enemy_value;//score
        int _my_score;//score
        int _enemy_score;//score


        public string globalstring;//to server

        public Image returnpicturebycard(playcard card)
        {
            switch (card._shape)
            {
                case ("D"):
                    switch (card._value)
                    {
                        case (1):
                            return global::cards_war.Properties.Resources.ace_of_diamonds;

                        case (2):
                            return global::cards_war.Properties.Resources._2_of_diamonds;

                        case (3):
                            return global::cards_war.Properties.Resources._3_of_diamonds;

                        case (4):
                            return global::cards_war.Properties.Resources._4_of_diamonds;

                        case (5):
                            return global::cards_war.Properties.Resources._5_of_diamonds;

                        case (6):
                            return global::cards_war.Properties.Resources._6_of_diamonds;

                        case (7):
                            return global::cards_war.Properties.Resources._7_of_diamonds;

                        case (8):
                            return global::cards_war.Properties.Resources._8_of_diamonds;

                        case (9):
                            return global::cards_war.Properties.Resources._9_of_diamonds;

                        case (10):
                            return global::cards_war.Properties.Resources._10_of_diamonds;

                        case (11):
                            return global::cards_war.Properties.Resources.jack_of_diamonds2;

                        case (12):
                            return global::cards_war.Properties.Resources.queen_of_diamonds2;

                        case (13):
                            return global::cards_war.Properties.Resources.king_of_diamonds2;

                        default:
                            return global::cards_war.Properties.Resources.card_back_red;

                    }

                case ("H"):
                    switch (card._value)
                    {
                        case (1):
                            return global::cards_war.Properties.Resources.ace_of_hearts;

                        case (2):
                            return global::cards_war.Properties.Resources._2_of_hearts;

                        case (3):
                            return global::cards_war.Properties.Resources._3_of_hearts;

                        case (4):
                            return global::cards_war.Properties.Resources._4_of_hearts;

                        case (5):
                            return global::cards_war.Properties.Resources._5_of_hearts;

                        case (6):
                            return global::cards_war.Properties.Resources._6_of_hearts;

                        case (7):
                            return global::cards_war.Properties.Resources._7_of_hearts;

                        case (8):
                            return global::cards_war.Properties.Resources._8_of_hearts;

                        case (9):
                            return global::cards_war.Properties.Resources._9_of_hearts;

                        case (10):
                            return global::cards_war.Properties.Resources._10_of_hearts;

                        case (11):
                            return global::cards_war.Properties.Resources.jack_of_hearts2;

                        case (12):
                            return global::cards_war.Properties.Resources.queen_of_hearts2;

                        case (13):
                            return global::cards_war.Properties.Resources.king_of_hearts2;

                        default:
                            return global::cards_war.Properties.Resources.card_back_red;


                    }

                case ("C"):
                    switch (card._value)
                    {
                        case (1):
                            return global::cards_war.Properties.Resources.ace_of_clubs;

                        case (2):
                            return global::cards_war.Properties.Resources._2_of_clubs;

                        case (3):
                            return global::cards_war.Properties.Resources._3_of_clubs;

                        case (4):
                            return global::cards_war.Properties.Resources._4_of_clubs;

                        case (5):
                            return global::cards_war.Properties.Resources._5_of_clubs;

                        case (6):
                            return global::cards_war.Properties.Resources._6_of_clubs;

                        case (7):
                            return global::cards_war.Properties.Resources._7_of_clubs;

                        case (8):
                            return global::cards_war.Properties.Resources._8_of_clubs;

                        case (9):
                            return global::cards_war.Properties.Resources._9_of_clubs;

                        case (10):
                            return global::cards_war.Properties.Resources._10_of_clubs;

                        case (11):
                            return global::cards_war.Properties.Resources.jack_of_clubs2;

                        case (12):
                            return global::cards_war.Properties.Resources.queen_of_clubs2;

                        case (13):
                            return global::cards_war.Properties.Resources.king_of_clubs2;

                        default:
                            return global::cards_war.Properties.Resources.card_back_red;


                    }

                case ("S"):
                    switch (card._value)
                    {
                        case (1):
                            return global::cards_war.Properties.Resources.ace_of_spades2;

                        case (2):
                            return global::cards_war.Properties.Resources._2_of_spades;

                        case (3):
                            return global::cards_war.Properties.Resources._3_of_spades;

                        case (4):
                            return global::cards_war.Properties.Resources._4_of_spades;

                        case (5):
                            return global::cards_war.Properties.Resources._5_of_spades;

                        case (6):
                            return global::cards_war.Properties.Resources._6_of_spades;

                        case (7):
                            return global::cards_war.Properties.Resources._7_of_spades;

                        case (8):
                            return global::cards_war.Properties.Resources._8_of_spades;

                        case (9):
                            return global::cards_war.Properties.Resources._9_of_spades;

                        case (10):
                            return global::cards_war.Properties.Resources._10_of_spades;

                        case (11):
                            return global::cards_war.Properties.Resources.jack_of_spades2;

                        case (12):
                            return global::cards_war.Properties.Resources.queen_of_spades2;

                        case (13):
                            return global::cards_war.Properties.Resources.king_of_spades2;

                        default:
                            return global::cards_war.Properties.Resources.card_back_red;


                    }

                default:
                    return global::cards_war.Properties.Resources.card_back_red;



            }




        }//return picture by card .. (card its my class)

        public void generatebackcards()//make the card dinamicly
        {
            cardspack pack = new cardspack();//my class - 52 card(myclass to) 
            pack = pack.mix(pack);//{-->..|
            pack = pack.mix(pack);///---->mix 3 times - like a normal mix 
            pack = pack.mix(pack);//{---..|
            bool[] iflasttap = new bool[10];//to know if tap on this card 


            int x = 86;
            int y = 294;
            int j = 0;

            for (int i = 0; i < 11; i++)//make the photo - 11 photo
            {
                bool ifshow = true;

                PictureBox pictureBox = new PictureBox();
                if (i < 10)//to the first 10 photo
                {
                    pictureBox.Image = global::cards_war.Properties.Resources.card_back_red;
                    pictureBox.Location = new System.Drawing.Point(x, y);
                    pictureBox.Name = "pictureBox2";
                    pictureBox.Size = new System.Drawing.Size(103, 138);
                    pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                    pictureBox.TabIndex = 2;
                    pictureBox.TabStop = false;

                    this.Controls.Add(pictureBox);
                    x += pictureBox.Size.Width + 10;


                    pictureBox.Click += delegate(object sender, EventArgs e1)
                    {
                        
                        if (ifshow)
                        {//whan tap
                            /////////////////////////////////
                            playcard temp = pack._packcards[j];//get out the i card in the pack
                            pack._packcards[j] = null;
                            j += 1;
                            PictureBox p = (PictureBox)sender;
                            p.Image = returnpicturebycard(temp);//get the pgoto by card
                            p.Show();//show him 
                            ifshow = false;
                            //////////////////////////////////
                            _my_value = getValueOfCard(temp);//get the value from this card
                            Server.send(getprotocolcard(temp));//send to server



                        }
                        else
                        {
                          
                              pictureBox = (PictureBox)sender;//hide the red if tap again 
                              pictureBox.Image = global::cards_war.Properties.Resources.card_back_red;
                            
                          

                        }
                    };

                }
                else//11 photo - the blue 
                {
                    x = ((113 * 5) + (113 * 6)) / 2 - 20;
                    y -= (138 * 2) + 10;
                    _p = new PictureBox();//p is the global picture 
                    _p.Image = global::cards_war.Properties.Resources.card_back_blue;
                    _p.Location = new System.Drawing.Point(x, y);
                    _p.Name = "pictureBox2";
                    _p.Size = new System.Drawing.Size(103, 138);
                    _p.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                    _p.TabIndex = 2;
                    _p.TabStop = false;

                    this.Controls.Add(_p);
                }

            }
        }

        public playcard getcardbyname(string name)//get card by name 
        {
            string value ;
            string shape = name[3].ToString();
            string color ; 
            if (name[1] == '0')
            {
                value = name[2].ToString();
            }else
            {
                value = name[1].ToString() + name[2].ToString();
            }

            if (name[0].ToString() == "H" || name[0].ToString() == "D")
            {
                color = "red";
            }else{
                color = "black";
            }


            playcard temp = new playcard(Int32.Parse(value), shape, color);
            return temp;



        }

        public string getprotocolcard(playcard send)//get the protocol of the card 
        {

            string recv = "error";
            string sendmsg = "1";

            int value = send._value;

            if (value > 0 && value < 10)
            {
                sendmsg += "0" + value.ToString();
            }
            else if (value >= 10 && value < 14)
            {
                sendmsg += value.ToString();
            }
            
            else
            {
                return recv;
            }

            sendmsg += send._shape;
            return sendmsg;


        }

        public void ThreadServer()// the thread 
        {
            
            while (ifexit != true)
            {
                _recv = Server.recv();//read from .. 

                if (_recv[0] == '1')// if is cardcode
                {
                    playcard temp = getcardbyname(_recv);//get the card
                    Invoke((MethodInvoker)delegate {
                        _p.Image = returnpicturebycard(temp);//get the photo with invok 
                    });
                    _enemy_value = getValueOfCard(getcardbyname(_recv));//get enemy value 

                    if (_my_value > _enemy_value)//my value he in the tap 
                    {
                        _my_score += 1; //plus 1 to me 
                    }else if(_enemy_value > _my_value)//plus one Too him 
                    {
                        _enemy_score += 1;
                    }
                    else//draw
                    {
                        _my_score += 0;
                        _enemy_score += 0;
                    }
                    Invoke((MethodInvoker)delegate
                    {
                       score.Text = _my_score.ToString();//invok chencge the label score 
                       score.Refresh();
                    });

                }
                else if (_recv == "2000")//exit code 
                {
                    //print the score
                    MessageBox.Show("your score is :" + _my_score.ToString() + "\nenemy score is:" + _enemy_score.ToString(), "END OF GAME - BY SERVER");
                    ifexit = true;//yes we exit 
                    Server.close();//close server 
                    Environment.Exit(0);//exit app 
                }
            }
        }

        public CardsBoard()
        {
            Thread thread = new Thread(ThreadServer);//make the thread 
            InitializeComponent();//init 
            
            
            Server.connect();//connect 
            Server.send("0000");//send 2000 
            _recv = Server.recv();//recv 
            if (_recv == "0000")//if 2000 go on 
            {
                generatebackcards();//generate
                thread.Start();//start the thread
            }
            else
            {
                MessageBox.Show("error wite the other player", "ERROR");//error 
            }
        }

        public int getValueOfCard(playcard card)//get value 
        {

            if (card._value != 1)//if no ace
            {
                return card._value;
            }
            else//if ace 
            {
                return 14;//the higer value 
            }

        }

        private void button1_Click(object sender, EventArgs e)// if exit click 
        {
            Server.send("2000");//send 2000 - exit code 
            //print score 
            MessageBox.Show("your score is :" + _my_score.ToString() + "\nenemy score is:" + _enemy_score.ToString(), "END OF GAME");
            ifexit = true;//yes we exit
            Server.close();//close the server 
            Environment.Exit(0);//app exit 
        }

        private void button1_Click(object sender, FormClosingEventArgs e)//if x was clicked
        {
            //2000 to server - exit code 
            Server.send("2000");
            //show score 
            MessageBox.Show("your score is :" + _my_score.ToString() + "\nenemy score is:" + _enemy_score.ToString(), "END OF GAME");
            Server.close();//close the server 
            ifexit = true;//if exit = true 
            //its exit anyway 
        }

     







    }
}
