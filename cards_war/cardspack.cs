﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cards_war
{
    class cardspack
    {
        public playcard[] _packcards = new playcard[52];

        public cardspack()
        {

            string red = "red";
            string black = "black";

            string shapediamond = "D";
            string shapeheart= "H";
            string shapeclub = "C";//תלתן
            string shapespade = "S";//עלה


            for (int i = 0; i <= 12; ++i)
            {
                _packcards[i] = new playcard(i + 1, shapediamond, red);
                
            }

            for (int i = 13; i <= 25; ++i)
            {
                _packcards[i] = new playcard((i - 13) + 1, shapeheart, red);
            }

            for (int i = 26; i <= 38; ++i)
            {
                _packcards[i] = new playcard((i - 26) + 1, shapeclub, black);
            }

            for (int i = 39; i <= 51; ++i)
            {
                _packcards[i] = new playcard((i - 39) + 1, shapespade, black);
            }
        }


        public cardspack mix(cardspack recvpack)
        {
            Random r1 = new Random(((int)DateTime.Now.Ticks) * ((int)DateTime.Now.Ticks));

            Random r = new Random(((int)DateTime.Now.Ticks) * r1.Next());

            cardspack pack = recvpack;

            for (int i = 0; i < pack._packcards.Length; i++)
            {
                int j = r.Next(i);
                
                
                playcard k = pack._packcards[j];

                pack._packcards[j] = pack._packcards[i];
                pack._packcards[i] = k;
            }

            return pack;
        }
       



    }
}
